# README #

This [repository][1] contains my modest contribution to the free and open source [Java 8 / JavaFX 8][2] world.
It contains some source code and classes that I found useful and that may interest other developers.
The code has been tested and contains JUnit tests and demo classes.
The project can be build using Maven.   

### license ###

The source code is presented as is, with no warranties, and is licensed under the (free) BSD license.

### further information ###

Currently, there is no blog post coming with the project.

### Contribution guidelines ###

* Feel free to contribute to this project
* Code contribution must use (free) BSD License

[1]: https://bitbucket.org/lguberan/public_repo
[2]: http://docs.oracle.com/javase/8/javafx/get-started-tutorial/jfx-overview.htm