/*
 * Copyright (c) 2015-2016, Lucien Guberan Consulting. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *     * Neither the name of Lucien Guberan Consulting, any associated website, nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL LUCIEN GUBERAN CONSULTING BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Nota bene: 
 * This license is the standard BSD 3-clause license ("Revised BSD License").
 */
package com.guberan.fxlib;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.property.ListProperty;
import javafx.beans.property.MapProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SetProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.ObservableSet;

/**
 * Fx objects (objects with fx properties) added to the {@code FxRestorableBag} can be restored
 * later using the rollback() method, in a similar way to database transactions.<br/>
 * {@code FxRestorableBag} supports savepoints (saving intermediate state), similar
 * to nested transactions. List, Map or Set can be restored by the {@code FxRestorableBag} as well.
 * If the elements of a List, Map or Set are themselves fx objects that must be restorable, 
 * these elements must be added to the {@code FxRestorableBag}.
 * <p>
 * {@code FxRestorableBag} is useful when objects are edited and the user may cancel the changes.
 * <p>
 * Usage example:
 * <pre>
 * {@code
 * FxRestorableBag tx = new FxRestorableBag(fxObject);
 * ... // modification on the fxObject here
 * fxObject.niceProperty().set(newValue);
 * if (userCancelled)
 *    tx.rollback();
 * else
 *    tx.commit();
 * }
 * </pre>
 * <p>
 * A filter may be used to exclude certain properties from the "transaction".
 * Excluded (filtered out) properties will not be recorded and can not be
 * restored.<p>
 * <p>
 * Example for filtering out properties named "Address" :
 * <pre>
 * {@code
 * FxRestorableBag tx = new FxRestorableBag();
 * tx.setFilter(prop -> !"Address".equals(prop.getName()));
 * tx.addTxObjects(myFxObject1, myFxObject2);
 * }
 * </pre>
 * {@code savepointNb()} return the current nested transaction level. 0 : top level, 1 : first sub-level,
 * etc...<p>
 * <p>
 * <b>Architecture:</b><br/>
 * {@code FxRestorableBag} listen to property changes and record the <u>original</u> values in a
 * Map<Property, Object>.
 * On rollback, original values of fx properties are restored.
 * List, map or set properties are copied before the transaction starts so that a copy can be
 * restored later.
 * Note that it would be difficult to record every possible changes on lists, sets and maps.
 * <p>
 * {@code rollback()} restore the original values of fx properties.
 * <p>
 * {@code commit()} does nothing (since the changes already happened) except clearing the buffer and making
 * the FxRestorableBag ready for another usage.
 * <p>
 * Revision: 2,  2016-03-29
 */
public class FxRestorableBag implements ChangeListener<Object> 
{
	static final Logger log = LoggerFactory.getLogger(FxRestorableBag.class);
	
	// all properties this object listen to
	ArrayList<Property<?>> fxPropertyList;
		
	// filter to filter out specific fx properties
	Predicate<Property<?>> filter;

	// savepoints (contain saved data for possible restore)
	List<Map<Property<?>, Object>> savepoints;	
	
	// backup for lists, maps and set properties
	Map<Object, Object> propBackupMap;
	
	// flag, set if FxRestorableBag is currently in a roll back operation
	protected boolean insideRollback;
	

	/* ----------------------------------------------------------------------------------
	   static methods
	------------------------------------------------------------------------------------- */

	/**
	 * <code>isValidFxPropertyAccessor</code><br/>
	 * 
	 * Check if the given the Method is a valid accessor for an fx Property.<br>
	 * fx accessor methods have : <br/>
	 * - no parameters <br/>
	 * - a return type of type Property<br/>
	 * - a name which ends with "Property"<br/>
	 * - are not static<br/>
	 * 
	 * example of valid fx property accessor : <code>public SimpleStringProperty nameProperty();</code><br/>
	 * 
	 * @param m Method to check
	 * @return true if the method is a valid property accessor
	 */
	public static boolean isValidFxPropertyAccessor(Method m)
	{
		// property accessor method must have no parameter and must not be static
		if (m.getParameterTypes().length != 0 || Modifier.isStatic(m.getModifiers()))
			return false;
		
		// property accessor's name must end with "Property"
		String accessorName = m.getName();
		if (!accessorName.endsWith("Property"))
			return false;
		
		// return type must implement Property
		Class<?> retType = m.getReturnType();
		return ((retType != null) && Property.class.isAssignableFrom(retType));
	}
	

	
	/**
	 * <code>collectFxProperties</code> <br/>
	 * 
	 * collects all fx (writable) Properties.<br/>
	 * A filter may be provided.<br/>
	 * 
	 * The following example collect all fx properties of myDomainObject except
	 * the property "Address"<br/>
	 * <code>List<Property> properties = collectFxProperties(myDomainObject, prop -> !"Address".equals(prop.getName());</code>
	 * 
	 * @param obj		object that contains the fx Properties
	 * @param filter	optional property filter function or null
	 * @return an ArrayList containing all fx writable properties.
	 */
	public static List<Property<?>> collectFxProperties(Object obj, Predicate<Property<?>> filter)
	{
		List<Property<?>> fxPropList = new ArrayList<>();		
		
		// code resists to null obj, returns empty list of properties
		if (obj == null)
			return fxPropList;

		// collect all public methods that are valid property accessors
		Method[] publicMethods = obj.getClass().getMethods();
		
		for (Method m : publicMethods) {
			
			if (isValidFxPropertyAccessor(m)) {
				
				// invoke accessor method to get the property
				try {
					Property<?> p = (Property<?>) m.invoke(obj);
					if (p != null)									// property may be null if never initialised
						if (filter == null || filter.test(p))
							fxPropList.add(p);
					
				} catch (Exception ex) {
					
					log.warn("collecting Property failed for {}, obj '{}'.", m, obj, ex);
				}
			}
		}
	
		return fxPropList;
	}

	
	
	
	/* ----------------------------------------------------------------------------------
	   constructors
	------------------------------------------------------------------------------------- */

	
	/**
	 * constructor of a FxRestorableBag
	 * 
	 * The FxRestorableBag listen to all fx properties of the given beans
	 * and record the original values when a change happen.
	 * 
	 * The FxRestorableBag can rollback() all the changes on request.
	 * 
	 * @param objs all objects (with fx properties) that may be restored later
	 */
	public FxRestorableBag(Object ... objs)
	{
		// set if a rollback is currently happening
		insideRollback = false;
		
		// list of all fx properties
		fxPropertyList = new ArrayList<>();
		
		// map holding the copies of lists, maps or sets
		propBackupMap = new HashMap<Object, Object>();
		
		// initialize property filter
		filter = null;
		
		// initialize savepoints
		savepoints = new ArrayList<>();
		savepoints.add(new HashMap<Property<?>, Object>());
		
		// add all objects to transaction
		addTxObjects(objs);
	}
	
		


	/* ----------------------------------------------------------------------------------
	   various methods
	------------------------------------------------------------------------------------- */
	

	/**
	 * addTxObjects
	 * 
	 * Add more objects to the FxRestorableBag. Changes to the fx properties of these
	 * objects will be recorded and may be rolled back later. 
	 * 
	 * @param objs list of objects to add to the FxRestorableBag
	 */
	public void addTxObjects(Object... objs)
	{		
		// for all objects in transaction ...
		for (Object obj : objs) {
			
			// ... collect the fx properties		
			List<Property<?>> addPropList = collectFxProperties(obj, filter);
			
			if (!addPropList.isEmpty())
				fxPropertyList.addAll(addPropList);
			else
				log.info("obj {} of class {} does not have any writable properties.", obj, (obj==null) ? "" : obj.getClass());
	
			
			// listen to the changes of all fx properties
			for (Property<?> prop : addPropList) {
				
				backupIfListOrMapOrSet(prop);				
				prop.addListener(this);
			}			
		}
	}
	
	

	/**
	 * internal method : For maps, lists or sets, it's not easy to keep track of
	 * all changes and restore the original values later, therefore we make a
	 * backup of these objects in order to roll them back later.
	 * 
	 * @param prop  Property
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void backupIfListOrMapOrSet(Property<?> prop)
	{
		
		// if property is a List, keep a backup
		if (prop instanceof ListProperty) {
								
			ObservableList<?> originalList = ((ListProperty<?>) prop).getValue();
			List<?> backupList = (originalList == null) ? null : FXCollections.observableArrayList(originalList);

			// it's necessary to call System.identityHashCode()
			// because ListProperty.hashCode() depends on its content,
			// which means that adding or removing an element in the
			// list will change the hashcode (key) of the ListProperty
			// in the HashMap and we won't be able to find it anymore !
			propBackupMap.put(System.identityHashCode(prop), backupList);
					
		}
		
		// if property is a map, keep a backup
		else if (prop instanceof MapProperty) {
			
			ObservableMap originalMap = ((MapProperty) prop).getValue();					
			ObservableMap backupMap = null;
			
			if (originalMap != null) {
				// copy map content
				backupMap = FXCollections.observableHashMap();
				backupMap.putAll(originalMap);
			}
			
			propBackupMap.put(System.identityHashCode(prop), backupMap);	
		}		
		
		// if property is a set, keep a backup
		else if (prop instanceof SetProperty) {
			
			ObservableSet originalSet = ((SetProperty) prop).getValue();
			Set backupSet = null;
			
			if (originalSet != null) {
				// copy set content
				backupSet = FXCollections.observableSet();
				backupSet.addAll(originalSet);
			}
			
			propBackupMap.put(System.identityHashCode(prop), backupSet);	
		}		
	}

	
	
	/**
	 * Creates a savepoint, a point in the transaction to which it's possible to
	 * roll back (also called nested transaction).
	 * returns the savepoint level, a number also used as identifier
	 * 
	 * @return savepoint identifier (int, the nested level)
	 */
	public int savepoint()
	{
		// for all lists, maps or sets that changed in latest savepoint, make a
		// backup we can use later
		for (Property<?> prop : savepointMap().keySet()) {
			backupIfListOrMapOrSet(prop);			
		}

		// create a new map for Property-value storage
		savepoints.add(new HashMap<Property<?>, Object>());
		
		return getSavepointNb();
	}
	


	/**
	 * <code>getSavepointNb</code> <br/>
	 * return the number of savepoints (nested transaction), 0 is the topmost transaction
	 * 
	 * @return number of savepoints
	 */
	public int getSavepointNb()
	{
		return savepoints.size() -1;
	}
	
	
	
	/**
	 * <code>savepointMap</code> <br/>
	 * return the current (latest) "savePoint" map : a <Property, Original_Values> map which hold
	 * properties' original values.
	 * 
	 * @return latest savepoint map
	 */
	protected Map<Property<?>, Object> savepointMap() {
		
		return savepoints.get(getSavepointNb());
	}

	

	/**
	 * cleanup
	 * 
	 * remove property listener, clear the map of all recorded changes, reset the filter.
	 */
	public void cleanup() 
	{	
		// remove all listeners to the properties
		for (Property<?> prop : fxPropertyList) {			
			prop.removeListener(this);
		}
		
		// clear property list 
		fxPropertyList.clear();
		
		// clear backupMap
		propBackupMap.clear();
		
		// initialize property filter
		filter = null;

		// clear savepoints
		savepoints.clear();
		savepoints.add(new HashMap<Property<?>, Object>());
	}
	
	

	/**
	 * <code>commit</code> <br/>
	 * 
	 * clean up the transaction, doesn't really commit anything since data is already "committed"
	 */
	public void commit() 
	{	
		cleanup();
	}

		

	/**
	 * <code>rollback</code> <br/>
	 * 
	 * restore original values
	 */
	public void rollback() 
	{	
		// roll back to the outermost transaction
		rollback(0);
	}

	
	
	/**
	 * roll back to the specified savepoint.
	 *
	 * @param savepoint the savepoint to restore
	 * 
	 * @throws IndexOutOfBoundsException
	 *  if {@code savepoint} is out of range ({@code savepoint < 0 || savepoint > savepointNb()})
	 */
	public void rollback(int savepoint) 
	{	
		int level = getSavepointNb();
		
		if (savepoint < 0 || savepoint > level)
			throw new IndexOutOfBoundsException("Index: " + savepoint + ", Size: " + level); 
		
		// for each savepoint ...
		for (int i=level; i>=savepoint; i--) {
			
			// ... restore saved values
			rollback(savepoints.get(i));
			
			// remove savepoint unless it's the one we're rolling to.
			if (i > savepoint)
				savepoints.remove(i);
		}		
	}
	

	
	/**
	 * roll back, restoring the values saved in propertyValueMap
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected synchronized void rollback(Map<Property<?>, Object> propertyValueMap) 
	{	
		try {
			insideRollback = true;
		
			// for all properties in Map, restore the original values			
			for (Entry<Property<?>, Object> entry : propertyValueMap.entrySet()) {
				
				Property prop = entry.getKey();			
				log.debug("rolling back Property {} with value '{}'.", prop, entry.getValue());

				// the key is the fx Property, we just call setValue on it
				prop.setValue(entry.getValue());
				
				// if prop is list/map/set, put it back in propBackupMap
				backupIfListOrMapOrSet(prop);
			}
			
			// all values restored, fresh for restart
			propertyValueMap.clear();
		}
		finally {
			insideRollback = false;
		}
	}

	
	
	/**
	 * count the number of properties that were changed since the <code>savepoint</code> or in the entire
	 * transaction if <code>savepoint</code> is 0.<br/>
	 * Useful to figure out if any properties were actually changed.<br/>
	 * 
	 * returns number of properties changed.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code savepoint} is out of range ({@code savepoint < 0 || savepoint > savepointNb()})
	 */
	public int changesCount(int savepoint) 
	{
		int level = getSavepointNb();
		
		if (savepoint < 0 || savepoint > level)
			throw new IndexOutOfBoundsException("Index: " + savepoint + ", Size: " + level); 
		
		int count = 0;
		
		// for each savepoint, count the changes
		for (int i=level; i>=savepoint; i--)
			count += savepoints.get(i).size();
			
		return count;		
	}		
	
	

	/**
	 * Get current property filter.
	 * A filter may be used to exclude certain properties from the transaction.
	 * Excluded (filtered out) properties will not be recorded and can not be restored.
	 * 
	 * @return current property filter
	 */
	public Predicate<Property<?>> getFilter() {
		return filter;
	}

	
	
	/**
	 * Set property filter.
	 * 
	 * default property filter is null.
	 * A filter may be used to exclude certain properties from the transaction.
	 * Excluded (filtered out) properties will not be recorded and can not be restored.
	 */
	public void setFilter(Predicate<Property<?>> newFilter) {
		this.filter = newFilter;
	}
	
	
	
	/**
	 * listen to changed event
	 * 
	 * @param observable property that changed
	 * @param oldValue old value
	 * @param newValue new value
	 */
	@Override
	public synchronized void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) 
	{	
		// if we are currently rolling back (restoring values), we don't record the (reverse) change
		if (insideRollback)
			return;
				
		log.debug("register change for Property {}, value '{}'.", observable, oldValue);
				
		Property<?> prop = (Property<? extends Object>) observable;	
		
		if (prop instanceof ListProperty || prop instanceof MapProperty || prop instanceof SetProperty) {
			
			// handle the case of null list, map or set
			if (propBackupMap.containsKey(System.identityHashCode(prop)))
			{
				// a list or map was changed, get the backup we made and put it in the propValueMap so we can restore it later
				Object oldListOrMapOrSet = propBackupMap.get(System.identityHashCode(prop));				
				savepointMap().putIfAbsent(prop, oldListOrMapOrSet);
				propBackupMap.remove(System.identityHashCode(prop));
				
				log.debug("initial change for list/map/set Property was put in savepoint '{}'.", prop);
			}
			else {
				// list or map was already changed, original value was already saved in propValueMap
				log.debug("already? changed list/map/set Property was NOT put in savepoint '{}'.", prop);
			}
		}
		else {
		
			// a simple property was changed, record the old value so we can restore it later
			savepointMap().putIfAbsent(prop, oldValue);
		}
	}


	
	/**
	 * finalize
	 * 
	 * clean up before garbage collection
	 */
	@Override
	public void finalize() 
	{	
		cleanup();
	}
}

