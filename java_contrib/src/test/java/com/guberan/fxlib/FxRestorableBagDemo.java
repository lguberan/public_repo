/*
 * Copyright (c) 2015-2016, Lucien Guberan Consulting. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *     * Neither the name of Lucien Guberan Consulting, any associated website, nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL LUCIEN GUBERAN CONSULTING BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Nota bene: 
 * This license is the standard BSD 3-clause license ("Revised BSD License").
 */
package com.guberan.fxlib;

import java.text.NumberFormat;
import java.util.List;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.converter.FormatStringConverter;



/**
 * FxRestorableBagDemo
 * 
 * Simple demo of FxRestorableBag.
 * Create an editor for an fx object, restore the object if user clicked cancel.
 * 
 * Revision: 2,  2016-03-29
 */
public class FxRestorableBagDemo extends Application
{

	
	/* ----------------------------------------------------------------------------------
	   main and Application methods
	------------------------------------------------------------------------------------- */

	/**
	 * main entry point
	 * @param args
	 */
	public static void main(String[] args) 
	{
		launch(args);		
	}


	/**
	 * start application
	 * 
	 * Demo FxRestorableBag functionality
	 */
	@Override
	public void start(Stage primaryStage) 
	{
		// create demo object (Person in this case, nothing extravagant)
		Person person = new Person("Paul", "Smith", 34, 100.01);

		// create our editor dialog
		TinyEditorDialog editor = new TinyEditorDialog(person);

		// show the dialog
        Dialog<ButtonType> dialog = new Dialog<ButtonType>();
        dialog.setTitle("Demo FxRestorableBag");
        dialog.setResizable(true);
        dialog.setDialogPane(editor);
        dialog.showAndWait();		
 	}

	 
	/* ----------------------------------------------------------------------------------
	   edit Dialog 
	------------------------------------------------------------------------------------- */
	
	
	/**
	 * Tiny editor for Fx objects
	 */
	public static class TinyEditorDialog extends DialogPane
	{
		List<Property<?>> 		props;
		Object 					fxObject;
		SimpleIntegerProperty 	level = new SimpleIntegerProperty(0);
		FxRestorableBag 		tx;
		
		
		/**
		 * constructor of the editor
		 * 
		 * @param fxObject fx object to edit
		 */
		public TinyEditorDialog(Object fxObject) 
		{
			super();
			
			this.fxObject 	= fxObject;
			this.props 		= FxRestorableBag.collectFxProperties(fxObject, null);
			
			tx = new FxRestorableBag(fxObject);
			
			BorderPane bPane = new BorderPane();
			bPane.setTop(new Label("FxRestorableBag\nSave and restore javafx properties.\n\nex usage:\nFxRestorableBag tx = new FxRestorableBag(fxObject);\nfxObject.xyzProperty().set(newValue);...\nif (problem)\n   tx.rollback();\n\n \n"));
			bPane.setLeft(buildVBox());
			
			GridPane grid = buildPropertyGrid();
			
			Button ok = new Button("OK (SavePoint)");
			ok.setOnAction(e -> { level.set(tx.savepoint()); System.out.println("saved");});
	
			Button cancel = new Button("Cancel (Rollback)");
			cancel.setOnAction(e -> { tx.rollback(tx.getSavepointNb()-1);  level.set(tx.getSavepointNb()); System.out.println("restored"); });
			cancel.disableProperty().bind(Bindings.greaterThan(1, level));
	
			Label lbl = new Label();
			lbl.textProperty().bind(Bindings.format("tx level=%s", level));
			
			HBox hBox = new HBox(10.0, lbl, cancel, ok);
			hBox.setPadding(new Insets(15, 12, 15, 12));
			
			grid.add(hBox, 0, 6, 2, 1);
			bPane.setRight(grid);
			
			getButtonTypes().setAll(ButtonType.OK);
			setContent(bPane);
		}
	
	
	
		/**
		 * build a grid with a column for the labels and another for the editors
		 * @return GridPane with labels and editors
		 */
		public GridPane buildPropertyGrid()
		{
			GridPane grid = new GridPane();
			grid.setHgap(4);
			grid.setVgap(12);
			grid.setPadding(new Insets(10, 10, 10, 10));
			
			int index = 0;
			
			// for every property
			for (Property<?> prop : props)
			{
				//create an editor for property
				TextField edt = new TextField();
				GridPane.setHgrow(edt, Priority.SOMETIMES);
	
				// bind editor with property
				if (prop instanceof SimpleStringProperty) {
					edt.textProperty().bindBidirectional((SimpleStringProperty) prop);
				}
				else if (prop instanceof SimpleIntegerProperty) {
					edt.setTextFormatter(new TextFormatter<Integer>(new FormatStringConverter<Integer>(NumberFormat.getIntegerInstance())));
					edt.textProperty().bindBidirectional(prop, NumberFormat.getIntegerInstance());
				}
				else if (prop instanceof SimpleDoubleProperty) {
					edt.setTextFormatter(new TextFormatter<Double>(new FormatStringConverter<Double>(NumberFormat.getNumberInstance())));
					edt.textProperty().bindBidirectional(prop, NumberFormat.getNumberInstance());
				}
				else {
					edt.textProperty().set((prop.getValue() != null) ? prop.getValue().toString() : "<null>");
					edt.setDisable(true);
				}
	
				// create the label
				Label lbl = new Label(prop.getName());
				GridPane.setHalignment(lbl, HPos.RIGHT);			
				lbl.setLabelFor(edt);
				
				// add to the grid
				grid.add(lbl, 0, index);
				grid.add(edt, 1, index);
				
				index++;
			}
			return grid;
		}

		
		public VBox buildVBox()
		{
			VBox vbox = new VBox();
			vbox.setPrefWidth(200);
			vbox.setPadding(new Insets(10));
			vbox.setSpacing(8);
			for (Property<?> prop : props) {
				Label lbl = new Label();
				lbl.textProperty().bind(Bindings.format("%s", prop));
				vbox.getChildren().add(lbl);
			}
			vbox.setStyle("-fx-border-color: DARKGREY;");
			return vbox;
		}

	}
	
	

	/* -------------- inner class for test ----------------- */
	
	/**
	 * test class for fx Properties
	 */
	public static class Person
	{
		// constructor
		public Person(String firstName, String name, int age, double num) {
			firstNameProperty().set(firstName);
			nameProperty().set(name);
			ageProperty().set(age);
			numProperty().set(num);
			//cousinProperty().set(null);
		}
		
		// firstName
		private SimpleStringProperty firstNameProp = new SimpleStringProperty(this, "firstName");
		public SimpleStringProperty firstNameProperty() { return firstNameProp; }
				
		// name
		private SimpleStringProperty nameProp = new SimpleStringProperty(this, "name");
		public SimpleStringProperty nameProperty() { return nameProp; }

		// age
		private SimpleIntegerProperty ageProp = new SimpleIntegerProperty(this, "age");
		public SimpleIntegerProperty ageProperty() { return ageProp; }
		
		// num double
		private SimpleDoubleProperty numProp = new SimpleDoubleProperty(this, "num");
		public SimpleDoubleProperty numProperty() { return numProp; }

		// cousin		
		//private SimpleObjectProperty<Person> cousinProp = new SimpleObjectProperty<Person>(this, "cousin");
		//public SimpleObjectProperty<Person> cousinProperty() { return cousinProp; }
		

		@Override
		public String toString() {
			return String.format("%s %s (%d) %f", firstNameProperty().get(),
					nameProperty().get(), ageProperty().get(),
					numProperty().get());
		}
	}

}
