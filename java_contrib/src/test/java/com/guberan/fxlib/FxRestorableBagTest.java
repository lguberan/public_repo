/*
 * Copyright (c) 2015-2016, Lucien Guberan Consulting. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *     * Neither the name of Lucien Guberan Consulting, any associated website, nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL LUCIEN GUBERAN CONSULTING BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Nota bene: 
 * This license is the standard BSD 3-clause license ("Revised BSD License").
 */
package com.guberan.fxlib;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleSetProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;



/**
 * JUnit test class for FxRestorableBag
 */
public class FxRestorableBagTest {
	
	// ============================================================================================
	//   inner class used to test fx properties changes
	// ============================================================================================
	public static class FxObj 
	{		
		// simple constructor
		public FxObj() {
		}
			
		// constructor
		public FxObj(String s, boolean b, double d, int i, FxObj o, List<Color> l) {
			setName(s);
			setReady(b);
			setNum(d);
			setCount(i);
			setObj(o);
			if (l == null)
				setColors(null); 
			else 
				setColors(FXCollections.observableArrayList(l));
		}
		
		
		// name String
		public String getName() { return nameProp.get(); }
		public void setName(String newName) { nameProp.set(newName); }
		private SimpleStringProperty nameProp = new SimpleStringProperty(this, "name");
		public SimpleStringProperty nameProperty() { return nameProp; }
	    
			
		// ready boolean
		public boolean isReady() { return readyProp.get(); }
		public void setReady(boolean newValue) { readyProp.set(newValue); }
		private SimpleBooleanProperty readyProp = new SimpleBooleanProperty(this, "ready");
		public SimpleBooleanProperty readyProperty() { return readyProp; }


		// num double
		public double getNum() { return numProp.get(); }
		public void setNum(double newNum) { numProp.set(newNum); }
		private SimpleDoubleProperty numProp = new SimpleDoubleProperty(this, "num");
		public SimpleDoubleProperty numProperty() { return numProp; }

		// count int
		public int getCount() { return countProp.get(); }
		public void setCount(int newCount) { countProp.set(newCount); }
		private SimpleIntegerProperty countProp = new SimpleIntegerProperty(this, "count");
		public SimpleIntegerProperty countProperty() { return countProp; }

		// obj Object
		public FxObj getObj() { return objProp.get(); }
		public void setObj(FxObj newObj) { objProp.set(newObj); }
		private SimpleObjectProperty<FxObj> objProp = new SimpleObjectProperty<>(this, "obj");
		public SimpleObjectProperty<FxObj> objProperty() { return objProp; }

		// colors list of Color
		public ObservableList<Color> getColors() { return colorsProp.get(); }
		public void setColors(ObservableList<Color> newList) { colorsProp.set(newList); }
		private SimpleListProperty<Color> colorsProp = new SimpleListProperty<>(this, "colors");
		public SimpleListProperty<Color> colorsProperty() { return colorsProp; }

		// map map of Object
		private SimpleMapProperty<String, Object> map = new SimpleMapProperty<>(this, "map", FXCollections.observableHashMap());
		public SimpleMapProperty<String, Object> mapProperty() { return map; }
		
		// set set of Object
		private SimpleSetProperty<Object> set = new SimpleSetProperty<>(this, "set", FXCollections.observableSet());
		public SimpleSetProperty<Object> setProperty() { return set; }

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("FxObj [name=");
			builder.append(getName());
			builder.append(", ready=");
			builder.append(isReady());
			builder.append(", num=");
			builder.append(String.valueOf(getNum()));
			builder.append(", count=");
			builder.append(String.valueOf(getCount()));
			builder.append(", obj=");
			String s = (getObj() == null) ? "null" : getObj().getName();
			builder.append("@obj " + s);		
			builder.append(", colors=");
			builder.append(getColors());
			builder.append("]");
			return builder.toString();
		}
	}
	
		
	/**
	 * Return true if the two given objects o1 and o2 are equals.
	 * 
	 * @param o1  first object
	 * @param o2 second object
	 * @return true if o1 and o2 are equals.
	 */
	public static boolean equalsFx(FxObj o1, FxObj o2) 
	{
		if (o1 == o2) return true;
		if ((o1 == null) || (o2 == null)) return false;
		
		return (equals(o1.getName(), o2.getName()) &&
				equals(o1.isReady(), o2.isReady()) &&
				equals(o1.getNum(), o2.getNum()) &&
				equals(o1.getCount(), o2.getCount()) &&
				equals(o1.getObj(), o2.getObj()) &&
				equals(o1.getColors(), o2.getColors())
			);
	}
	

	/**
	 * Return true if the two given objects o1 and o2 are equals.
	 * 
	 * @param o1  first object
	 * @param o2 second object
	 * @return true if o1 and o2 are equals.
	 */
	public static boolean equals(Object o1, Object o2) 
	{
		return (o1 == o2) || ((o1 != null) && o1.equals(o2));
	}


	// ============================================================================================
	//   JUnit tests
	// ============================================================================================

	
	/**
	 * test the restoration of all type of properties (int, string, boolean, list ...)
	 */
	@Test
	public void testAllKindOfProperties() {
		
		FxObj o1 = new FxObj("name1", true, 1.1, 1, null, null);
		FxObj o2 = new FxObj("name2", false, 2.2, 2, o1, null);
		FxObj o3 = new FxObj("name3", false, 3.3, 3, o2, Arrays.asList(Color.BLACK, Color.BLUE, Color.RED, Color.YELLOW));

		FxRestorableBag tx = new FxRestorableBag(o1, o2, o3);

		// change o1
		o1.setName("newName_o1");
		o1.setReady(false);
		o1.setNum(111111.11111);
		o1.setCount(11111111);
		o1.setObj(o3);
		o1.setColors(FXCollections.observableArrayList(Arrays.asList(Color.BLUE, Color.RED, Color.YELLOW)));

		
		// change o2
		o2.setName("newName_o2");
		o2.setReady(true);
		o2.setNum(222.2222);
		o2.setCount(22222);
		o2.setObj(o3);
		o2.setColors(FXCollections.observableArrayList(Arrays.asList(Color.BLUE, Color.YELLOW)));

		// change o3
		o3.setName("newName_o3");
		o3.setReady(true);
		o3.setNum(33.3333);
		o3.setCount(33333);
		o3.setObj(o1);
		o3.getColors().remove(1);
		o3.getColors().remove(Color.YELLOW);
		o3.getColors().add(Color.BLACK);

		
		// will restore all properties
		tx.rollback();
		
		// all properties must have original values
		assertTrue(equalsFx(o1, new FxObj("name1", true, 1.1, 1, null, null)));		
		assertTrue(equalsFx(o2, new FxObj("name2", false, 2.2, 2, o1, null)));			
		assertTrue(equalsFx(o3, new FxObj("name3", false, 3.3, 3, o2, Arrays.asList(Color.BLACK, Color.BLUE, Color.RED, Color.YELLOW))));		
	}

	
	
	/**
	 * test the filtering of properties
	 */
	@Test
	public void testFilter() {
		
		FxObj o1 = new FxObj("name1", true, 1.1, 1, null, null);
		FxObj o2 = new FxObj("name2", false, 2.2, 2, o1, null);
		FxObj o3 = new FxObj("name3", false, 3.3, 3, o1, Arrays.asList(Color.BLACK, Color.BLUE, Color.RED, Color.YELLOW));

		FxRestorableBag tx = new FxRestorableBag();
		tx.setFilter(prop -> !"name".equals(prop.getName())); // name property will not be restored
		tx.addTxObjects(o1, o2, o3);

		// o2 is changed
		o2.setObj(o3);
		o2.setName("newName");
		o2.setReady(true);
		o2.setNum(999999);

		// will rollback all properties except the "name" property which was filtered out
		tx.rollback();
		
		// the name must be "newName" now, all other properties being restored
		FxObj expected = new FxObj("newName", false, 2.2, 2, o1, null);
		
		//System.out.println(o2.toString());
		//System.out.println(expected.toString());
		
		assertTrue(equalsFx(o2, expected));		
	}
	
	
	/**
	 * test nested transactions / savepoints
	 */
	@Test
	public void testNestedTransactions() {

		FxObj o1 = new FxObj("name1", true, 1.1, 1, null, null);
		FxObj o2 = new FxObj("name2", false, 2.2, 2, o1, null);
		FxObj o3 = new FxObj("name3", false, 3.3, 3, o1, Arrays.asList(Color.BLACK, Color.BLUE, Color.RED, Color.YELLOW));

		FxRestorableBag tx = new FxRestorableBag(o1, o2, o3);
		
		o1.setName("name1_step1");
		o1.setCount(11);
		
		o2.setName("name2_step1");
		o2.setNum(22.22);
		o2.setColors(FXCollections.observableArrayList(Arrays.asList(Color.BLUE, Color.YELLOW)));
		
		o3.setName("name3_step1");
		o3.getColors().remove(1);
		
		// intermediate savepoint
		int sp1 = tx.savepoint();
		assertEquals(sp1, 1);
		
		o1.setName("name1_step2");
		o1.setCount(111);
		
		o2.setName("name2_step2");
		o2.setNum(222.222);
		o2.setColors(FXCollections.observableArrayList(Arrays.asList(Color.YELLOW)));
		
		o3.setName("name3_step2");
		o3.getColors().remove(1);
	
		// one more savepoint
		int sp2 = tx.savepoint();
		assertEquals(sp2, 2);
		assertEquals(tx.getSavepointNb(), 2);
		
		o1.setName("name1_step3");
		o1.setCount(1111);
		
		o2.setName("name2_step3");
		o2.setNum(2222.2222);

		
		int sp3 = tx.savepoint();
		assertEquals(sp3, 3);
		
		o1.setName("name1_step4");
		o1.setNum(1111111);
		
		o2.setName("name2_step4");
		o2.setNum(22222.22222);
		o2.setColors(FXCollections.observableArrayList(Arrays.asList(Color.ORANGE)));
	
		// rollback to previous state
		tx.rollback(sp3);
		assertEquals(tx.getSavepointNb(), 3);
		
		// check values
		assertTrue(equalsFx(o1, new FxObj("name1_step3", true, 1.1, 1111, null, null)));
		assertTrue(equalsFx(o2, new FxObj("name2_step3", false, 2222.2222, 2, o1, FXCollections.observableArrayList(Arrays.asList(Color.YELLOW)))));
		assertTrue(equalsFx(o3, new FxObj("name3_step2", false, 3.3, 3, o1, FXCollections.observableArrayList(Arrays.asList(Color.BLACK, Color.YELLOW)))));

		// rollback 2 steps !
		tx.rollback(sp1);
		assertEquals(tx.getSavepointNb(), 1);
		// System.out.println("++++ "  + tx.getSavepointNb());
		
		assertTrue(equalsFx(o1, new FxObj("name1_step1", true, 1.1, 11, null, null)));
		assertTrue(equalsFx(o2, new FxObj("name2_step1", false, 22.22, 2, o1, FXCollections.observableArrayList(Arrays.asList(Color.BLUE, Color.YELLOW)))));
		assertTrue(equalsFx(o3, new FxObj("name3_step1", false, 3.3, 3, o1, Arrays.asList(Color.BLACK, Color.RED, Color.YELLOW))));

		// change again
		o2.setName("changed again");
		o2.setColors(FXCollections.observableArrayList(Arrays.asList(Color.ORANGE)));
		
		// rollback again
		tx.rollback(sp1);
		assertTrue(equalsFx(o1, new FxObj("name1_step1", true, 1.1, 11, null, null)));
		assertTrue(equalsFx(o2, new FxObj("name2_step1", false, 22.22, 2, o1, FXCollections.observableArrayList(Arrays.asList(Color.BLUE, Color.YELLOW)))));
		assertTrue(equalsFx(o3, new FxObj("name3_step1", false, 3.3, 3, o1, Arrays.asList(Color.BLACK, Color.RED, Color.YELLOW))));
		
		//System.out.println("++++ "  + tx.getSavepointNb());

		tx.rollback();
		assertEquals(tx.getSavepointNb(), 0);
		
		// original values
		FxObj o1_expect = new FxObj("name1", true, 1.1, 1, null, null);
		FxObj o2_expect = new FxObj("name2", false, 2.2, 2, o1, null);
		FxObj o3_expect = new FxObj("name3", false, 3.3, 3, o1, Arrays.asList(Color.BLACK, Color.BLUE, Color.RED, Color.YELLOW));
		
		assertTrue(equalsFx(o1, o1_expect));
		assertTrue(equalsFx(o2, o2_expect));
		assertTrue(equalsFx(o3, o3_expect));
		
		// start again ???
		o1.setName("name1_step1");
		o1.setColors(FXCollections.observableArrayList(Arrays.asList(Color.BLUE, Color.YELLOW)));
		o2.setColors(FXCollections.observableArrayList(Arrays.asList(Color.BLUE, Color.ORANGE)));
		tx.rollback();
		
		assertTrue(equalsFx(o1, o1_expect));
		assertTrue(equalsFx(o2, o2_expect));
		assertTrue(equalsFx(o3, o3_expect));
		
		tx.cleanup();		
	}
	
	
	
	/**
	 * test planty of savepoints with maps and sets
	 */
	@Test
	public void testTransactions() 
	{	
		FxObj o1 = new FxObj("name1", true, 1.1, 1, null, null);
		FxObj o2 = new FxObj("name2", false, 2.2, 2, o1, null);
		FxObj o3 = new FxObj("name3", false, 3.3, 3, o1, Arrays.asList(Color.BLACK, Color.BLUE, Color.RED, Color.YELLOW));

		FxRestorableBag tx = new FxRestorableBag(o1, o2, o3);
		o3.setColors(FXCollections.observableArrayList(new ArrayList<Color>()));
		o3.mapProperty().put("key0", "value0");
		o3.setProperty().add("setValue0");
		
		// make plenty of savepoints
		for (int i=1; i<20; i++) {
			o1.nameProperty().set("name_" + i);
			o1.countProperty().set(i);
			o3.getColors().add(Color.BLUE);
			o3.mapProperty().put("key0", "value" + i);
			o3.mapProperty().put("key" + i, "value" + i);
			o3.setProperty().add("setValue"+i);
			tx.savepoint();
		}
		
		tx.rollback(15);
		assertEquals(tx.getSavepointNb(), 15);
		assertTrue(equalsFx(o1, new FxObj("name_15", true, 1.1, 15, null, null)));		
		assertTrue(o3.getColors().size() == 15);
		assertEquals("value15", o3.mapProperty().get("key0"));
		assertEquals("value15", o3.mapProperty().get("key15"));
		assertEquals("value14", o3.mapProperty().get("key14"));
		assertEquals(16, o3.mapProperty().size());
		
		assertEquals(16, o3.setProperty().size());
		assertEquals(true, o3.setProperty().contains("setValue15"));
		assertEquals(true, o3.setProperty().contains("setValue0"));
		
		// rollback a lot
		for (int i=14; i>5; i--) {
			tx.rollback(i);
			assertTrue(equalsFx(o1, new FxObj("name_" + i, true, 1.1, i, null, null)));
			assertTrue(o3.getColors().size() == i);
			
			// map
			assertEquals("value"+i, o3.mapProperty().get("key0"));
			assertEquals("value"+i, o3.mapProperty().get("key"+i));
			assertEquals(i+1, o3.mapProperty().size());
			
			// set
			assertEquals(true, o3.setProperty().contains("setValue0"));
			assertEquals(i+1, o3.setProperty().size());
		}
		
		assertEquals(tx.getSavepointNb(), 6);
		
		// commit again
		for (int i=7; i<18; i++) {
			o1.nameProperty().set("name_" + i);
			o1.countProperty().set(i);
			o3.getColors().add(Color.BLUE);
			o3.mapProperty().put("key0", "value" + i);
			o3.mapProperty().put("key" + i, "value" + i);
			o3.setProperty().add("setValue"+i);
			tx.savepoint();
		}

		System.out.println("XXX " + tx.getSavepointNb());
		assertEquals(tx.getSavepointNb(), 17);
		
		o1.nameProperty().set("name_" + 18);
		o1.countProperty().set(18);

		// rollback again
		for (int i=17; i>0; i--) {
			tx.rollback(i);
			System.out.println(o1.toString());
			assertTrue(equalsFx(o1, new FxObj("name_" + i, true, 1.1, i, null, null)));
			assertTrue(o3.getColors().size() == i);
			
			// map
			assertEquals("value"+i, o3.mapProperty().get("key0"));
			assertEquals("value"+i, o3.mapProperty().get("key"+i));
			assertEquals(i+1, o3.mapProperty().size());
			
			// set
			assertEquals(true, o3.setProperty().contains("setValue0"));
			assertEquals(i+1, o3.setProperty().size());
		}

		// rollback total		
		tx.rollback();
		
		FxObj o1_expect = new FxObj("name1", true, 1.1, 1, null, null);
		FxObj o2_expect = new FxObj("name2", false, 2.2, 2, o1, null);
		FxObj o3_expect = new FxObj("name3", false, 3.3, 3, o1, Arrays.asList(Color.BLACK, Color.BLUE, Color.RED, Color.YELLOW));

		assertTrue(equalsFx(o1, o1_expect));
		assertTrue(equalsFx(o2, o2_expect));
		assertTrue(equalsFx(o3, o3_expect));
	}


	/**
	 * test addTxObjects() and changesCount()
	 */
	@Test
	public void addTxObjectsTest() 
	{
		FxObj o1 = new FxObj("name1", true, 1.1, 1, null, null);
		FxObj o2 = new FxObj("name2", false, 2.2, 2, o1, null);
		FxObj o3 = new FxObj("name3", false, 3.3, 3, o2, Arrays.asList(Color.BLACK, Color.BLUE, Color.RED, Color.YELLOW));
		
		FxRestorableBag tx = new FxRestorableBag(o1);

		assertEquals(0, tx.changesCount(0)); // no changes yet
		
		// change o1
		o1.setName("newName_o1");
		assertEquals(1, tx.changesCount(0));	 // 1 change
		
		o1.setReady(false);
		o1.setNum(111111.11111);
		o1.setCount(11111111);
		o1.setObj(o3);
		o1.setColors(FXCollections.observableArrayList(Arrays.asList(Color.BLUE, Color.RED, Color.YELLOW)));

		assertEquals(6, tx.changesCount(0)); // 6 changes
		
		o1.setName("newName_o1111");		 // change on same property "name" of o1
		assertEquals(6, tx.changesCount(0)); // still 6 changes (same property changed twice)
		
		tx.addTxObjects(o2, o3);

		// change o2
		o2.setName("newName_o2");
		o2.setReady(true);
		o2.setNum(222.2222);
		o2.setCount(22222);
		o2.setObj(o3);
		o2.setColors(FXCollections.observableArrayList(Arrays.asList(Color.BLUE, Color.YELLOW)));

		assertEquals(12, tx.changesCount(0)); // 12 changes
		
		int sp = tx.savepoint(); // first savepoint
		assertEquals(1, sp);     
		
		o1.setColors(FXCollections.observableArrayList(Arrays.asList(Color.BLUE, Color.RED)));
		assertEquals(13, tx.changesCount(0)); // 13 changes since the beginning
		assertEquals(1, tx.changesCount(1));  //  1 change since savepoint 1
		
		// will restore all properties
		tx.rollback();
		
		// all properties must have original values
		assertTrue(equalsFx(o1, new FxObj("name1", true, 1.1, 1, null, null)));		
		assertTrue(equalsFx(o2, new FxObj("name2", false, 2.2, 2, o1, null)));			
		assertTrue(equalsFx(o3, new FxObj("name3", false, 3.3, 3, o2, Arrays.asList(Color.BLACK, Color.BLUE, Color.RED, Color.YELLOW))));		
	}


	@Test(expected=IndexOutOfBoundsException.class)
	public void changesCountNegativeTest1() 
	{
		FxObj o1 = new FxObj("name1", true, 1.1, 1, null, null);
		FxObj o2 = new FxObj("name2", false, 2.2, 2, o1, null);
		FxRestorableBag tx = new FxRestorableBag(o1, o2);
		tx.changesCount(-4);
	}

	@Test(expected=IndexOutOfBoundsException.class)
	public void changesCountNegativeTest2() 
	{
		FxObj o1 = new FxObj("name1", true, 1.1, 1, null, null);
		FxObj o2 = new FxObj("name2", false, 2.2, 2, o1, null);
		FxRestorableBag tx = new FxRestorableBag(o1, o2);
		tx.changesCount(-1);
	}

	@Test(expected=IndexOutOfBoundsException.class)
	public void rollbackNegativeTest1() 
	{
		FxObj o1 = new FxObj("name1", true, 1.1, 1, null, null);
		FxObj o2 = new FxObj("name2", false, 2.2, 2, o1, null);
		FxRestorableBag tx = new FxRestorableBag(o1, o2);
		o1.setName("newName_o1");
		tx.rollback(7);
	}

	@Test(expected=IndexOutOfBoundsException.class)
	public void rollbackNegativeTest2() 
	{
		FxObj o1 = new FxObj("name1", true, 1.1, 1, null, null);
		FxObj o2 = new FxObj("name2", false, 2.2, 2, o1, null);
		FxRestorableBag tx = new FxRestorableBag(o1, o2);
		o1.setName("newName_o1");
		tx.rollback(-1);
	}

	@Test
	public void rollbackTest() 
	{
		FxObj o1 = new FxObj("name1", true, 1.1, 1, null, null);
		FxObj o2 = new FxObj("name2", false, 2.2, 2, o1, null);
		FxRestorableBag tx = new FxRestorableBag(o1, o2);
		o1.setName("newName1_o1");
		tx.savepoint();
		o1.setName("newName2_o1");
		tx.savepoint();
		o1.setName("newName3_o1");
		tx.savepoint();
		o1.setName("newName4_o1");
		
		assertEquals(o1.getName(), "newName4_o1");
		tx.rollback(3);
		assertEquals(o1.getName(), "newName3_o1");
		tx.rollback(3);
		tx.rollback(3);
		assertEquals(o1.getName(), "newName3_o1");
		tx.rollback(0);
		assertEquals(o1.getName(), "name1");
		tx.rollback(0);
		tx.rollback(0);
		assertEquals(o1.getName(), "name1");
		
		o1.setName("newName");
		tx.cleanup();
		
		assertEquals(o1.getName(), "newName");
		o1.setName("newName4_o1");
	}

	@Test
	public void nullEmptyListTest() 
	{
		FxObj o1 = new FxObj("name1", true, 1.1, 1, null, null);
		
		FxRestorableBag tx = new FxRestorableBag(o1);
		
		assertEquals(o1.getColors(), null); // null list
		
		ObservableList<Color> l1 = FXCollections.observableArrayList(Arrays.asList(Color.BLUE, Color.RED, Color.YELLOW));
		ObservableList<Color> l2 = FXCollections.observableArrayList(Arrays.asList(Color.BLUE, Color.RED, Color.YELLOW));
		o1.setColors(l1);
		assertEquals(o1.getColors(), l2);
		
		tx.savepoint();	
		assertEquals(o1.getColors(), l2);
		
		o1.getColors().remove(0);
		tx.rollback(1);
		
		assertEquals(o1.getColors(), l2);
		
		o1.setColors(FXCollections.observableArrayList(Arrays.asList())); // empty list
		
		int sp = tx.savepoint();

		o1.getColors().add(Color.RED);
		assertEquals(o1.getColors(), FXCollections.observableArrayList(Arrays.asList(Color.RED)));
		o1.getColors().add(Color.BLUE);
		assertEquals(o1.getColors(), FXCollections.observableArrayList(Arrays.asList(Color.RED, Color.BLUE)));
		
		tx.rollback(sp);
		assertEquals(o1.getColors(), FXCollections.observableArrayList(Arrays.asList()));
		
		tx.rollback(1);
		assertEquals(o1.getColors(), l2);
		
		tx.rollback();
		assertEquals(o1.getColors(), null);		
	}

	@Test
	public void nullObjectTest() 
	{
		FxObj o1 = new FxObj("name1", true, 1.1, 1, null,  Arrays.asList(Color.BLACK, Color.BLUE));
		
		FxRestorableBag tx = new FxRestorableBag(null, o1 , null);
		tx.addTxObjects(null, null, null, new Object());
		o1.setName("bad name");
		o1.getColors().add(Color.RED);
		tx.rollback();
		
		assertTrue(equalsFx(o1, new FxObj("name1", true, 1.1, 1, null,  Arrays.asList(Color.BLACK, Color.BLUE))));		
	}
	
	@Test
	public void equalsFxTest() {
		FxObj o1 = new FxObj("name3", false, 3.3, 3, null, Arrays.asList(Color.BLACK, Color.BLUE, Color.RED, Color.YELLOW));
		FxObj o2 = new FxObj("name3", false, 3.3, 3, null, Arrays.asList(Color.BLACK, Color.BLUE, Color.RED, Color.YELLOW));
		assertTrue(equalsFx(o1, o2));
	}

	
}
